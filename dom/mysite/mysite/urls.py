from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
    url(r'^blog/', include('microblog.urls')),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': 'static'})
)
