# Create your views here.
from django.shortcuts import render, get_object_or_404
from datetime import datetime
from django.utils import timezone
from models import Wpis
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

def detail(request, wpis_id):
    aWpis = get_object_or_404(Wpis, pk=wpis_id)
    return render(request, 'microblog/detail.html', {'wpis': aWpis})
    
def base(request):
    wpisy = Wpis.objects.all()
    return render(request, 'microblog/base.html', {'wpisy': wpisy})
    
def dodajwpis(request):
    wpisy = Wpis.objects.all()
    if request.user.is_authenticated():
        return render(request, 'microblog/add.html')
    else:
        info = u"By dodawac wpisy musisz sie zalogowac!"
        return render(request, 'microblog/base.html', {'info':info, 'wpisy':wpisy})
    
def dodawaniewpisu(request):
    wpisy = Wpis.objects.all()
    if request.user.is_authenticated():
        if request.method == "POST":
            nowyWpis = Wpis()
            nowyWpis.pub_date = timezone.now()
            nowyWpis.text = request.POST.get('text', 0)
            nowyWpis.author = request.user
            nowyWpis.save()
            info = "Dodano nowy wpis!"
        return render(request, 'microblog/base.html', {'info':info, 'wpisy':wpisy})
    else:
        info = "Musisz sie zalogowac by dodawac wpisy!"
        return render(request, 'microblog/base.html', {'info':info, 'wpisy':wpisy})

def zaloguj(request):
    wpisy = Wpis.objects.all()
    if not request.user.is_authenticated():
        return render(request, 'microblog/zaloguj.html')
    else:
        informacja = u"Zalogowales sie juz!"
        return render(request, 'microblog/base.html', {'info':informacja, 'wpisy': wpisy})
    
def logowanie(request):
    wpisy = Wpis.objects.all()
    if not request.user.is_authenticated():
        username = request.POST['login']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                informacja = u"Zalogowales sie."
                return render(request, 'microblog/base.html', {'info':informacja, 'wpisy': wpisy})
            else:
                info = 'Konto nieaktywne'
                return render(request, 'microblog/zaloguj.html', {'info':info})
        else:
            info2 = u"Zly uzytkownik lub haslo!"
            return render(request, 'microblog/zaloguj.html', {'info':info2})
    else:
        informacja = u"Jestes juz zalogowany!"
        return render(request, 'microblog/zaloguj.html', {'info':informacja})
        
def zarejestruj(request):
    wpisy = Wpis.objects.all()
    if not request.user.is_authenticated():
        return render(request, 'microblog/zarejestruj.html')
    else:
        informacja = u"Juz sie zarejestrowales!"
        return render(request, 'microblog/base.html', {'info':informacja, 'wpisy': wpisy})


def rejestracja(request):
    wpisy = Wpis.objects.all()
    if not request.user.is_authenticated():
        nazwa = request.POST['user_name']
        haslo = request.POST['user_password']
        haslo1 = request.POST['user_password1']
        if (haslo == haslo1):
            if (nazwa != "") and (haslo != ""):
                try:
                    uzytkownik = User.objects.create_user(nazwa, '',haslo)
                    uzytkownik.save()
                    informacja = u"Zarejestrowales sie!"
                    return render(request, 'microblog/base.html', {'info':informacja, 'wpisy': wpisy})
                except:
                    informacja = u"Uzytkownik juz istnieje!"
                    return render(request, 'microblog/zarejestruj.html', {'info':informacja, 'wpisy': wpisy})
            else:
                informacja = u"Nie wpisano loginu lub hasla"
                return render(request, 'microblog/zarejestruj.html', {'info':informacja})
        else:
            informacja = u"Hasla nie zgadzaja sie!"
            return render(request, 'microblog/zarejestruj.html', {'info':informacja})
    else:
        informacja = u"Juz jestes zarejestrowany!"
        return render(request, 'microblog/base.html', {'info':informacja, 'wpisy': wpisy})
     
def wyloguj(request):
    wpisy = Wpis.objects.all()
    if request.user.is_authenticated():
        logout(request)
        # Redirect to a success page.
        informacja = u"Wylogowales sie!"
        return render(request, 'microblog/base.html', {'info':informacja, 'wpisy': wpisy})
    else:
        info = u"Nie jestes zalogowany!"
        return render(request, 'microblog/base.html', {'info':info, 'wpisy': wpisy})
