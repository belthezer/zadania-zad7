from django.conf.urls import patterns, url
from django.views.generic import TemplateView
import views

urlpatterns = patterns('',
    url(r'detail/(?P<wpis_id>\d+)/$', views.detail),
    url(r'^$', views.base, name='base'),
    url(r'dodajwpis/$', views.dodajwpis, name="dodajwpis"),
    url(r'dodawaniewpisu/$', views.dodawaniewpisu, name="dodawaniewpisu"),
    url(r'zaloguj/$', views.zaloguj, name="zaloguj"),
    url(r'logowanie/$', views.logowanie, name="logowanie"),
    url(r'zarejestruj/$', views.zarejestruj, name="zarejestruj"),
    url(r'rejestracja/$', views.rejestracja, name="rejestracja"),
    url(r'wyloguj/$', views.wyloguj, name="wyloguj"),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': 'static'})
)
