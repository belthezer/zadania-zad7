from django.db import models
from django.utils import timezone

# Create your models here.

class Wpis(models.Model):
    text = models.CharField(max_length=400)
    pub_date = models.DateTimeField('termin publikacji')
    author = models.CharField(max_length=30)

    def __unicode__(self):
        return self.text

    def published_today(self):
        now = timezone.now()
        time_delta = now - self.pub_date
        return time_delta.days == 0
